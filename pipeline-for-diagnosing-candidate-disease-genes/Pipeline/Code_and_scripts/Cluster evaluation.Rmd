---
title: "Cluster evaluation"
author: "Toke Heerkens"
date: "16 september 2019"
output: word_document
---
```{r package install, include=FALSE}

#install.packages("ggplot2", "factoextra", "fpc", "magrittr")

```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library("ggplot2")
library("factoextra")
library("magrittr")
GenePanelCataloge <- read.csv("..\\Necessities\\Geneid_with_Phenotype.csv", header = T, row.names = 1)
# Function for creating the evaluation plots
Cluster_visualisation <-  function(cluster_model, tot_clusters, plots) {
  # this is a generict function that works with a hierarchis clusted object.
  # argument explenation
  # - cluster_model: the object that is outputed by the R function (hclust) used for creating the plots
  # - tot_clusters: the total number of clusters that have been used tho do the clustering.  used in the title of the plots
  # - plots: this is an temporary enviromant where the created figures wil be saved in. this is becouse in R a function can only return one object. 
  #   so in order to be able to see the plots you need to store them temporary in a enviromant.

# generate a sillouette plot of the cluster object
plots$sil <- fviz_silhouette(cluster_model, print.summary = F, main= paste("silhouette plot of", tot_clusters, "clusters"), ggtheme = theme_minimal())
# generate a cluster plot 
plots$clust <-fviz_cluster(cluster_model, ellipse.type = "convex", ggtheme = theme_minimal(),
             labelsize = 0, main= paste("Cluster plot of", tot_clusters ,"clusters \n plotted on the first two dimentions of the data"))

}
```


The first step is to load the data
In order to do so you need to fill in the location and filename 

be aware that if your using windows the "\\"" used by the file manager is also the default escape character of R. To not run into any errors all single "\\" should be changed to "\\\\"

```{r loading_data}
# fill in the file location and filename within the "" in the load argument
load(paste0("..\\Outputs\\",Phenotype,"\\",Phenotype,"_coregmatrix.RData"))
# rename the object so its dinamic for the rest of the program
Genepannel_Cor_matrix <- geneCorZMatrix
# removing the original in order to not have duplicate infromation stored
remove(geneCorZMatrix)

```


Once the Gene panel is loaded you need to have an idear on how many clusters are in the selected Gene panel. The problem with deciding on the amount of clusters is that it is more of a feeling matter then a science. Meaning that one might interpret it as 3 clusters but someone else might say its 4. This is why the script will also run the chosen number of cluster +1 and -1. This douse mean one requirement the selected number of cluster must me equal of higher then 2.
There are ways to help you chose the "right" number of clusters one of them is making a heatmap of the coregulation. And the other is the show a the amount of cluster that are the furthers apart from a random point distribution (gap statistic [1]) 


```{r Choosingclustersize ,fig.width=13, fig.height=13}

heatmap(Genepannel_Cor_matrix, col = c(rep("dodgerblue", 2), colorRampPalette(c("dodgerblue", "white","firebrick3"))(900),rep("#CD2626", 16)), cexRow = 0.4, cexCol = 0.4)

```

```{r gap statistic plot}
# plot for the gap statistics
fviz_nbclust(Genepannel_Cor_matrix, hcut, method = "gap_stat", linecolor = "orange", verbose=F)
```

pleas fill in the chosen amount of clusters
```{r selected cluster size}
# fill in the chosen amount of clusters.
Cluster_Amount = 2

# N-1 and N+1 parameters
CA_min = Cluster_Amount-1
CA_plus = Cluster_Amount+1
```


Clustering of the three options n, n-1 and n+1
```{r clustering}
# clustering the chosen clusters
hclust_CA <- Genepannel_Cor_matrix %>%
  scale() %>%
  eclust("hclust", k = Cluster_Amount, graph = FALSE)

# clustering the chosen clusters - 1
hclust_CA_min <- Genepannel_Cor_matrix %>%
  scale() %>%
  eclust("hclust", k = CA_min, graph = FALSE)


# clustering the chosen clusters + 1 
hclust_CA_plus <- Genepannel_Cor_matrix %>%
  scale() %>%
  eclust("hclust", k = CA_plus, graph = FALSE)
```


For the evaluation each amount of clusters will be provided with three different plots. the first one is a cluster plot. this is a 2D representation of the data whereby the datapoint are labelled base on the cluster they belong to. The second is a dendrogram this shows the hierarchical relations of the different data entries. The last image is a silhouette plot, this plot gives some information on how close a certain data point is to a neighboring cluster. its scaled form 1 to -1 with 1 meaning that it is far away from any neighboring cluster and -1 being really close to neighboring cluster. This means that you're looking for values above one. But also have to take the other two plots into consideration.
```{r Evaluating}
plots_env<-new.env()
# plotting the figures for the evaluation of the chosen amount of clusters minus 1 if the cluster size is not equal to one
if (CA_min == 1) {  cat("chosen clustersize results in the minus 1 calculation to end up at 1 and you can't run clustering on 1 cluster")
} else {  Cluster_visualisation(hclust_CA_min, CA_min, plots_env)
          plot(plots_env$clust)
          plot(plots_env$sil)
          }


# plotting the figures for the evaluation of the chosen amount of clusters
Cluster_visualisation(hclust_CA, Cluster_Amount, plots_env)
plot(plots_env$clust)
plot(plots_env$sil)


# plotting the figures for the evaluation of the chosen amount of clustersplus 1
Cluster_visualisation(hclust_CA_plus, CA_plus, plots_env)
plot(plots_env$clust)
plots_env$sil
```

now that you have evaluated the clusters you can determining  which one is the "best". this is all well and good but having these images isn't telling you what is inside of the created clusters. that is where the next part comes in. it will give all the gene id for each cluster
but you first have to tell what the cluster amount was that is the "best" options: "minus", "chosen", "plus"

```{r what is inside a cluster}
selection = "chosen"

if (selection == "minus"){
  cluster_object = hclust_CA_min
} else {if (selection == "chosen") {
  cluster_object = hclust_CA
} else {if (selection == "plus"){
  cluster_object = hclust_CA_plus}
else {stop("pleas provide one of the posible options [minus, chosen, plus]")}}}


anotationtable <- data.frame(Gene_id = character(),
                             Gene_name = character(),
                             Cluster = integer(),
                             Phenotype = character(),
                             stringsAsFactors = FALSE)

for (i in 1: max(cluster_object$cluster)) {
  GenesInCluster = cluster_object$labels[cluster_object$cluster == i]
  for (gene in GenesInCluster){
    CatalogeEntery<- GenePanelCataloge[GenePanelCataloge$gene_id == gene,]
    if ("Distonia" %in% CatalogeEntery$panel_1 & "Myoclonus" %in% CatalogeEntery$panel_2 ) {
      pheno <- "MyoclonusDistonia"
    } else {if ("Distonia" %in% CatalogeEntery$panel_1) {
      pheno <- "Distonia"
    } else {if ("Myoclonus" %in% CatalogeEntery$panel_2){
      pheno <- "Myoclonus"
    } else {pheno <- "unpanneld"}}}

  anotationtable <- rbind(anotationtable, data.frame(Gene_id = gene, Gene_name= CatalogeEntery$gene_name, Cluster=i, Phenotype = pheno))
  }
  
}
rm(i, pheno, selection)

write.csv(anotationtable, file =paste0("..\\Outputs\\",Phenotype,"\\",Phenotype, "_anotation.csv"))

```

```{r gene network tabel creation}
# fully automated creation of of a source, target, zscore tabel

#the basis for the table
networkframe <- data.frame(Source = character(),
                           Target = character(),
                           zscore = double(),
                           stringsAsFactors = FALSE)


#filling the table
for (i in c(1 :nrow(Genepannel_Cor_matrix))) {
  source <- rownames(Genepannel_Cor_matrix)[i]
  for(j in c(1: ncol(Genepannel_Cor_matrix))){
    target <- colnames(Genepannel_Cor_matrix)[j]
    if (source == target) {
      next
    }else {if (j < i){
      zscore <- Genepannel_Cor_matrix[j,i]
    }
      zscore <- Genepannel_Cor_matrix[i,j]
    }
    if (target %in% networkframe$Source & source %in% networkframe$Target) {
      next
    }else {
    networkframe <- rbind(networkframe, data.frame(Source = source, Target= target, Zscore = zscore))
    }
  }
}
rm(source, target, zscore, i, j)

write.csv(networkframe, file =paste0("..\\Outputs\\",Phenotype,"\\",Phenotype,"_networkframe.csv"))

```


#### Reference list

[1]  Robert Tibshirani, Guenther Walther, Trevor Hastie.  Estimating the number of clusters in a data set via the gap statistic
Doi: 10.1111/1467-9868.00293
consulted on: 17-09-2019
