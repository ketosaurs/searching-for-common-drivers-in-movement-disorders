
library(readr,quietly=T)
library(parallel, quietly=T)

# Reading the gene pannel
case_table <- read.table(paste0("Pipeline",OSsep,"Input",OSsep,Analysis_name,"GenePanel.txt"), row.names = 1, header=T)
case_table <- case_table[1]
GenesOfInterest <- row.names(case_table)

# load the iegenvector matrix into a temparary table
table_tmp <- read_delim(paste0("Pipeline",OSsep,"Necessities",OSsep,"eigenvectors_1588.txt"),
               delim = "\t", quote = "",)


# remove the gene names for the matrix so that it becoumes compleaty numeric
eigenVectors <- as.matrix(table_tmp[, -1])

# add the genemanes as rownames so that it is still know what genes belong to what values
rownames(eigenVectors) <- table_tmp[, 1][[1]]
# remove the temparary table to clean up unasasary memory
rm(table_tmp)

# calculate the mean of the eigenvector matrix
eigenVectors.colMean <- apply(eigenVectors, 2, mean)
# calculate the standard deviation
eigenVectors.colSd <- apply(eigenVectors, 2, sd)

# normelize the eigen matrix over the PCA compontents
eigenVectorsPcNorm <- t((t(eigenVectors) - eigenVectors.colMean) / eigenVectors.colSd)


# calculate the mean of the normalized matrix
eigenVectorsPcNorm.rowMean <- apply(eigenVectorsPcNorm, 1, mean)
# calculate the standard deviation of the normiliezed matrix
eigenVectorsPcNorm.rowSd <- apply(eigenVectorsPcNorm, 1, sd)
# normalize the PCA normilized matrix over the genes.
eigenVectorsPcNormGeneNorm <- (eigenVectorsPcNorm - eigenVectorsPcNorm.rowMean) / eigenVectorsPcNorm.rowSd




# create a subset of the genes
eigenVectorsPcNormGeneNormSubset <-eigenVectorsPcNormGeneNorm[GenesOfInterest, ]

# generate a named matrix with the dimentions of the amount of selected genes.
geneCorZMatrix <-
    matrix(0,
           nrow = nrow(eigenVectorsPcNormGeneNormSubset),
           ncol = nrow(eigenVectorsPcNormGeneNormSubset),
           dimnames = list(rownames(eigenVectorsPcNormGeneNormSubset),rownames(eigenVectorsPcNormGeneNormSubset))
          )

# calculates the coregulation between the genes and fills the matrix from the above codechunks
for (i in 1:nrow(eigenVectorsPcNormGeneNormSubset)) {
    #parSapply(cl, 1:nrow(eigenVectorsPcNormGeneNormSubset), function(i){
    #cat(i); cat("\n")
    # for the current gene in the iteration take all its PC values and store them temporaly
    gene <- eigenVectorsPcNormGeneNormSubset[i, ]
    geneCorZ <- numeric(length = nrow(eigenVectorsPcNormGeneNormSubset))
    for (j in 1:nrow(eigenVectorsPcNormGeneNormSubset)) {
        # If it is a diagonal set the value to 0
        if (i == j) {
            geneCorZMatrix[i, j] <- 0
        # if not the diagonal calculate the coregulation between the two genes
        } else {
            corRes <-
                cor.test(gene,
                         eigenVectorsPcNormGeneNormSubset[j, ],
                         method = "pearson")
            # only looking of one end of the ttest
            z <- qnorm((corRes$p.value / 2), lower.tail = F)
            if (corRes$estimate < 0) {
                z <- z * -1
            }
            geneCorZ[j] <- z
        }
    }
    geneCorZMatrix[i, ] <- geneCorZ
}
#create a file to store all output data
dir.create(paste0("Pipeline",OSsep,"Outputs",OSsep, Analysis_name), showWarnings = F)
#save the data
save(geneCorZMatrix, file =paste0("Pipeline",OSsep,"Outputs",OSsep, Analysis_name,OSsep,Analysis_name ,"_coregmatrix.RData"))
# remove all variable exept Analysis_name case_table and eigenvectors
rm(list=setdiff(ls(), c("Analysis_name", "case_table", "eigenVectors", "OSsep")))
